package com.ad60.employeedir.controller;

/**
 * This will implement a contract that is defined by the {@link BaseController}
 *
 * Created by ktadesse on 7/30/16.
 */
public abstract class BaseControllerImpl <MODEL> extends AndroidBean <MODEL> implements BaseController<MODEL>{

    /**
     * Model represents the state of the view this controller is managing.
     *
     * @return Null when {@link #modelType()} returns null.
     *
     * This logic is out from the Subclasses and centralized here
     *
     */
    @Override
    public MODEL getModel() {
        return  super.getModel();
    }
}
