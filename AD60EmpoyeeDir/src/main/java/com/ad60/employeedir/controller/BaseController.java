package com.ad60.employeedir.controller;

/**
 * This is a base controller class that will define a contract that can be used by its child
 * controllers. This is specially dealing with the specific MODEL that is being used at the moment
 *
 * Created by ktadesse on 7/30/16.
 */
public interface BaseController <MODEL> {

    MODEL getModel();


}
