package com.ad60.employeedir.controller;

import com.ad60.employeedir.Util.ClassTypes;
import com.ad60.employeedir.employee.EmployeeControllerImpl;
import com.ad60.employeedir.employee.EmployeeModel;

/**
 * This is the BEAN class that will return all the new instances of the Model we are
 * trying to instantiate
 *
 * We will use Dagger 2 to inject the class here
 *
 * Created by ktadesse on 7/30/16.
 */
public abstract class AndroidBean <MODEL> {

    /**
     * <p>
     * A model that can represent anything passed onto it
     * </p>
     */
    private MODEL mModel;

    /**
     * Subclasses of {@link AndroidBean} will inherit this class to manipulate their data-model
     */
    public MODEL getModel() {
        return mModel;
    }

    /**
     * Subclasses will inherit this for telling what data-type-name they are controlling
     */
    public abstract ClassTypes modelType();

    /**
     * Initiate the data model using normal constructor
     */
    public void initiateModel() {
        /**
         * @see EmployeeControllerImpl#modelType() for its implementation
         */
        switch (modelType()) {

            case EMPLOYEEMODEL:
                mModel = (MODEL) new EmployeeModel();
                break;
        }
    }
}
