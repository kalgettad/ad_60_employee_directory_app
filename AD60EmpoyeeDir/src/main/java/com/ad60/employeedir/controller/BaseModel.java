package com.ad60.employeedir.controller;

/**
 * This will contain a model that will have information we need with in the parent or
 * child fragments.
 *
 * Created by ktadesse on 7/30/16.
 */
public class BaseModel<T> {

    /**
     * This flag will track the STATE of dependency injection we need in our fragments
     */
    private boolean isDependecyInjected;

    /**
     * @param dependecyInjected this sets the dependency flag on or off based on
     *                          passed boolean values
     */
    public void setDependecyInjected(boolean dependecyInjected) {
        this.isDependecyInjected = dependecyInjected;
    }

    /**
     * Returns the state of the dependecy injection as True or False
     */
    public boolean isDependecyInjected() {
        return isDependecyInjected;
    }
}
