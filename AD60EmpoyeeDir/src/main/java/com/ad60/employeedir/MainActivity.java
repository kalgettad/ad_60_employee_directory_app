package com.ad60.employeedir;

import com.ad60.employeedir.Util.Constants;

/**
 * This is the main activity that will be used to control data and the fragments it host.
 *
 * Created by ktadesse on 7/30/16.
 */
public class MainActivity extends MVCActivity{

    /**
     * @param tag the tag of the fragment that will be used to start it and adding it to the
     *            stack when necessary
     * @param object the data that we may want to pass into the fragment that will be used by it
     *
     */
    public void startFragment(String tag, Object object) {
        super.startFragment(tag, object);
    }

    @Override
    public void onBackPressed() {
        /**
         * This can be modified depending on our requirement because our app is fragment based
         * app and we may have a login screen that once the user logs in we are not suppose to
         * show it {@link onBackPressed()} because that will be a horrible UX to show a login after the
         * user has already thought they are logged in.
         *
         * Below is a good example how we can dodge the BULLET of showing the login screen again
         *
         *
         * This logic depends on the developer and the complexity of the app, this is just one
         * simple example.
         *
         */
        if (getFragmentManager().getBackStackEntryCount() > 1 && getFragmentManager().findFragmentByTag(Constants.FRAGMENT_TAG_LOGIN) == null) {

            /**
             * If the the fragment back stack count is larger than 1 and the login fragment is not added to the stack
             * this if{} block will let us navigate backwards through our app.
             */
            getFragmentManager().popBackStack();

        } else {
            /**
             * <p>
             *     Background the app to Android HOME screen because we only have on activity
             *     and if we pop it off the stack the next available activity is a.
             * </p>
             */
            super.onBackPressed();
        }
    }
}
