package com.ad60.employeedir.mock;

/**
 * This is just a simple mock that will imitate the api response we get from the backend
 * For this project this mock will suffice
 *
 * Created by ktadesse on 7/30/16.
 */
public class MockedEmployeeData {

    public static final String BOB_MOCKED_DATA = "{\n" +
            "  \"Success\": true, \n" +
            "  \"SessionCreated\": true, \n" +
            "  \"ProfileModel\": {\n" +
            "    \"Id\": \"AD60_134455_gf_5\", \n" +
            "    \"EName\": \"Bob\",\n" +
            "    \"ELName\": \"Roger\",\n" +
            "    \"Age\": \"32\",\n" +
            "    \"Zip\": \"63131\",\n" +
            "    \"City\": \"NYC\",\n" +
            "    \"Role\": \"HR\",\n" +
            "  },\n" +
            "}";

    public static final String GARRY_MOCKED_DATA = "{\n" +
            "  \"Success\": true, \n" +
            "  \"SessionCreated\": true, \n" +
            "  \"ProfileModel\": {\n" +
            "    \"Id\": \"AD60_156455_RT_5\", \n" +
            "    \"EName\": \"Garry\",\n" +
            "    \"ELName\": \"Busey\",\n" +
            "    \"Age\": \"62\",\n" +
            "    \"Zip\": \"63156\",\n" +
            "    \"City\": \"NC\",\n" +
            "    \"Role\": \"Actor\",\n" +
            "  },\n" +
            "}";
}
