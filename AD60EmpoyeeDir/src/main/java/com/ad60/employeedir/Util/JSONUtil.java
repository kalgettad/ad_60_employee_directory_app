package com.ad60.employeedir.Util;

import com.ad60.employeedir.employee.EmployeeModel;
import com.ad60.employeedir.employee.ProfileModel;

/**
 * TODO:
 * This is just to easily access data for the demo. This will never be the way its done
 * because we will have back end responses for this purpose
 *
 * Created by ktadesse on 7/31/16.
 */
public class JSONUtil {

    public static EmployeeModel generateBobData() {

        ProfileModel bobProfile = new ProfileModel();
        EmployeeModel bobModel = new EmployeeModel();
        bobModel.setSucess(true);
        bobModel.setSessionCreated(true);
        bobProfile.setEmployeeAge("65");
        bobProfile.setEmployeeCity("Boynton Beach");
        bobProfile.setEmployeeFirstName("Bob");
        bobProfile.setEmployeeId("1234_B");
        bobProfile.setEmployeeLastName("Rogers");
        bobProfile.setEmployeeRole("HR");
        bobProfile.setEmployeeZip("33431");
        bobModel.setProfile(bobProfile);

        return bobModel;

    }
    public static EmployeeModel generateGarryData() {

        ProfileModel garryProfile = new ProfileModel();
        EmployeeModel garryModel = new EmployeeModel();
        garryModel.setSucess(true);
        garryModel.setSessionCreated(true);
        garryProfile.setEmployeeAge("62");
        garryProfile.setEmployeeCity("Miami");
        garryProfile.setEmployeeFirstName("Garry");
        garryProfile.setEmployeeId("1465_G");
        garryProfile.setEmployeeLastName("Busey");
        garryProfile.setEmployeeRole("Actor");
        garryProfile.setEmployeeZip("33456");
        garryModel.setProfile(garryProfile);

        return garryModel;

    }
}
