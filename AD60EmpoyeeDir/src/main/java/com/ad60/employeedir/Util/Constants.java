package com.ad60.employeedir.Util;

/**
 * A class that will hold static constant instances we need all over the app
 *
 * Created by ktadesse on 7/30/16.
 */
public class Constants {


    public static final String FRAGMENT_TAG_LOGIN = "_TAG_LOGIN";
    public static final String FRAGMENT_TAG_EMPLOYEE_LIST = "_TAG_EMPLOYEE_LIST" ;
    public static final String FRAGMENT_TAG_EMPLOYEE_DETAIL = "_TAG_EMPLOYEE_DETAIL";

}
