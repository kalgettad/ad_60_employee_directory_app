package com.ad60.employeedir.Util;

import com.ad60.employeedir.employee.EmployeeModel;
import com.ad60.employeedir.fragment.EmployeesListFragment;

/**
 * This is an {@link Enum} class that holds all the name of the classes and will be used for
 * dagger 2 injection in the {@link com.ad60.employeedir.controller.AndroidBean}
 *
 * Created by ktadesse on 7/30/16.
 */
public enum ClassTypes {

    EMPLOYEEMODEL(EmployeeModel.class.getSimpleName().toString()), EMPLOYEE_LIST_FRAGMENT(EmployeesListFragment.class.getSimpleName().toString());

    String clsssTypes;

    ClassTypes(String classTypes) {
        this.clsssTypes = classTypes;
    }

    @Override
    public String toString() {
        return this.clsssTypes;
    }
}
