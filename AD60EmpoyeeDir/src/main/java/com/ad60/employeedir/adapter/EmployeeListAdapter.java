package com.ad60.employeedir.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ad60.employeedir.R;
import com.ad60.employeedir.Util.JSONUtil;
import com.ad60.employeedir.employee.EmployeeModel;
import com.ad60.employeedir.employee.ProfileModel;

import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * This is an adapter class that will load the list of employees with their necessary information
 *
 * Created by ktadesse on 7/30/16.
 */
public class EmployeeListAdapter extends BaseAdapter{

    private List<EmployeeModel> employeeList;

    public EmployeeListAdapter(List<EmployeeModel> employeesList) {
        this.employeeList = employeesList;
    }
    @Override
    public int getCount() {
        return employeeList.size();
    }

    @Override
    public Object getItem(int position) {
        return employeeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            /**
             * Inflate the View to load elements of a single row
             */
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.employee_item, parent, false);
            /**
             * Inject dependency injection pertaining to the UI that this adapter is tied to
             *
             */
            /**
             * Set the tag of the view using the {@link EmployeeListAdapterViewHolder} just injected
             */
            convertView.setTag(new EmployeeListAdapterViewHolder(convertView));
        }
        /**
         * Cast the ViewHoler to the {@link EmployeeListAdapterViewHolder} so we can access member variable
         * of that class
         */
        EmployeeListAdapterViewHolder employeeListAdapterViewHolder = (EmployeeListAdapterViewHolder) convertView.getTag();
        /**
         * Start using the view injections here by setting all the list of the names of the employees
         *
         */
        ProfileModel profileModel = employeeList.get(position).getProfile();
        //get the firstname
        String firstName = profileModel.getEmployeeFirstName();
        // set the first and last name
        employeeListAdapterViewHolder.nameView.setText(firstName + "" + profileModel.getEmployeeLastName());
        //Set the name indicator
        employeeListAdapterViewHolder.nameSymbolView.setText(firstName.substring(0,1));
        /**
         * Return the view
         */
        return convertView;
    }

    /**
     * This class will hold the individual Employee list element details
     *
     * Created by ktadesse on 7/30/16.
     */
    public class EmployeeListAdapterViewHolder {

        @Bind(R.id.row_layout) LinearLayout rowLayout;
        @Bind(R.id.name_view)   TextView nameView;
        @Bind(R.id.photo_symbol_view)   TextView nameSymbolView;
        @Bind(R.id.photo_view)  ImageView photoView;


        public EmployeeListAdapterViewHolder(View view) {

            ButterKnife.bind(this, view);
        }
    }
}
