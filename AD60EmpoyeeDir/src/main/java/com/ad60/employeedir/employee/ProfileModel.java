package com.ad60.employeedir.employee;

/**
 * This a separate class just to demonstrate different JSON response bodies
 *
 * Created by ktadesse on 7/30/16.
 */
public class ProfileModel {

    private String employeeId;
    private String employeeFirstName;
    private String employeeLastName;
    private String employeeAge;
    private String employeeZip;
    private String employeeCity;
    private String employeeRole;

    public String getEmployeeAge() {
        return employeeAge;
    }

    public String getEmployeeCity() {
        return employeeCity;
    }

    public String getEmployeeFirstName() {
        return employeeFirstName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }

    public String getEmployeeRole() {
        return employeeRole;
    }

    public String getEmployeeZip() {
        return employeeZip;
    }

    public void setEmployeeAge(String employeeAge) {
        this.employeeAge = employeeAge;
    }

    public void setEmployeeCity(String employeeCity) {
        this.employeeCity = employeeCity;
    }

    public void setEmployeeFirstName(String employeeFirstName) {
        this.employeeFirstName = employeeFirstName;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setEmployeeLastName(String employeeLastName) {
        this.employeeLastName = employeeLastName;
    }

    public void setEmployeeRole(String employeeRole) {
        this.employeeRole = employeeRole;
    }

    public void setEmployeeZip(String employeeZip) {
        this.employeeZip = employeeZip;
    }
}

