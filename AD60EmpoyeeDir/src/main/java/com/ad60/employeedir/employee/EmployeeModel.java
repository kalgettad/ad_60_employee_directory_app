package com.ad60.employeedir.employee;

import com.ad60.employeedir.controller.BaseModel;

/**
 * This is the EmployeeDetailViewHolder model class that have all the necessary data to be acted upon
 *
 * Created by ktadesse on 7/30/16.
 */
public class EmployeeModel extends BaseModel {

    public static  String LOG_TAG = EmployeeModel.class.getSimpleName();

    private ProfileModel profile;
    
    private boolean sucess;
    
    private boolean isSessionCreated;

    public ProfileModel getProfile() {
        return profile;
    }

    public void setProfile(ProfileModel profile) {
        this.profile = profile;
    }

    public void setSessionCreated(boolean sessionCreated) {
        isSessionCreated = sessionCreated;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public boolean isSessionCreated() {
        return isSessionCreated;
    }

    public boolean isSucess() {
        return sucess;
    }
}
