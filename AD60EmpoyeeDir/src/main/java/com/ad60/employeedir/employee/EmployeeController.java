package com.ad60.employeedir.employee;

import com.ad60.employeedir.controller.BaseController;

import java.util.List;

/**
 * This is an employee controller interface that will define a contract as to how we should interact
 * with {@link EmployeeModel} in different parts of the app as it is defined by our UML
 *
 * Created by ktadesse on 7/30/16.
 */
public interface EmployeeController extends BaseController <EmployeeModel> {

    /**
     * TODO:
     * This will parse the list of the project for this app only. This is for demo purposes only
     *
     * Usually we populate this files from the api response from the server
     */
    List<EmployeeModel>  getEmployeesList();
}
