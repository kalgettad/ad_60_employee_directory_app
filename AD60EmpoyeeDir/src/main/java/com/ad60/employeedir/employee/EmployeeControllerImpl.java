package com.ad60.employeedir.employee;

import com.ad60.employeedir.Util.ClassTypes;
import com.ad60.employeedir.Util.JSONUtil;
import com.ad60.employeedir.controller.BaseControllerImpl;
import java.util.ArrayList;
import java.util.List;

/**
 * This class will implement a contract that will define by the {@link EmployeeController} interface
 * This should be completely STATELESS -- NOT EVEN STATIC FINAL FIELDS!
 *
 * If we need to define static final fields we will use the {@link com.ad60.employeedir.Util.Constants}
 * class
 *
 * Created by ktadesse on 7/30/16.
 */
public class EmployeeControllerImpl extends BaseControllerImpl <EmployeeModel> implements EmployeeController{

    /**
     * TODO:
     * This block is because we don't have a back end service for our app. When we do have support
     * for it and we are using api calls to populate the list this will all be taken care of
     * in the Retrofit callbacks.
     *
     */
    @Override
    public List<EmployeeModel> getEmployeesList() {
        /**
         * Initiate  the model type here--meaning, tell the {@link com.ad60.employeedir.controller.AndroidBean}
         * to initiate the corresponding Data model type.
         */
        initiateModel();

        List<EmployeeModel> employeeModels = new ArrayList<>();
        /**
         * TODO: this is for demo purposes please integrate Retrofit
         *
         * Manually parse two employee datas
         *
         */
        employeeModels.add(JSONUtil.generateBobData());
        employeeModels.add(JSONUtil.generateGarryData());

        return employeeModels;
    }

    /**
     * Return the data-model type
     */
    @Override
    public ClassTypes modelType() {
        return ClassTypes.EMPLOYEEMODEL;
    }
}
