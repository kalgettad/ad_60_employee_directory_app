package com.ad60.employeedir.module;

import com.ad60.employeedir.employee.EmployeeModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This is the model module for injection using dagger 2
 *
 * Created by ktadesse on 7/30/16.
 */
@Module
public class ModelModule {

    ModelModule() {}

    @Provides
    @Singleton
    EmployeeModel provideEmployeeModel() {
        /**
         * Remember here we have a problem, because every time we run into this code we will be
         * building a new instance
         */
        return new EmployeeModel();
    }
}
