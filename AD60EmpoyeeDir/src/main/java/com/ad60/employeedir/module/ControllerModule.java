package com.ad60.employeedir.module;

import com.ad60.employeedir.employee.EmployeeController;
import com.ad60.employeedir.employee.EmployeeControllerImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This is a way of an injection for controllers
 *
 * Created by ktadesse on 7/30/16.
 */
@Module
public class ControllerModule {
    /**
     * For now we don't need any dependency for the controllers and we can
     * inject them using their default constructors
     *
     */

    @Provides
    @Singleton
    EmployeeController  provideEmployeeController() {
        /**
         * Use the interface for return type and return the Implementation
         */
        return new EmployeeControllerImpl();
    }
}
