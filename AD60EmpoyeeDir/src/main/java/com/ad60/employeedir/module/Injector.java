package com.ad60.employeedir.module;

import android.view.View;

/**
 * <p>
 *     This is an injector class that will inject dependencies when we need them.
 *     Additional purpose is once injected the class will remain with one instance
 *     for the entire life cycle of the class. This way we can avoid creating many instances
 *     of classes and invade memory -- especially for the objects we utilize globally.
 *
 * </p>
 */
public class Injector {


    /**
     * For this purpose we only need the injection of  {@link View}
     * Which is basic injection we need for {@link butterknife.ButterKnife} to function
     */
    private View view;

    public Injector(){}

    public Injector(View view){
        this.view = view;
    }


    public AppComponent getmViewHolderComponet() {
        /**
         * This can be expanded depending on the requirements we need. If we need more modules,
         * we can define more modules and define their parameters.
         */
        return DaggerAppComponent.builder().modelModule(new ModelModule()).controllerModule(new ControllerModule()).build();

    }
}
