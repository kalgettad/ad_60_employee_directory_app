package com.ad60.employeedir.module;

import com.ad60.employeedir.adapter.EmployeeListAdapter;
import com.ad60.employeedir.fragment.EmployeesListFragment;

import javax.inject.Singleton;


/**
 * This is the component that will hold injection points
 *
 * Basically this is the injector interface used for injection in Dagger 2
 *
 * For more understanding:
 * @see <a herf = https://guides.codepath.com/android/Dependency-Injection-with-Dagger-2></a>
 *
 * Created by ktadesse on 7/30/16.
 */
@Singleton
@dagger.Component(modules = { ModelModule.class, ControllerModule.class})
public interface AppComponent {


    void inject(EmployeesListFragment employeesListFragment);

    void inject(EmployeeListAdapter employeeListAdapter);

    void inject(EmployeesListFragment.EmployeesListViewHolder employeesListViewHolder);
}
