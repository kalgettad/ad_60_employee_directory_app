package com.ad60.employeedir;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.ad60.employeedir.Util.Constants;
import com.ad60.employeedir.fragment.EmployeeDetailFragment;
import com.ad60.employeedir.fragment.EmployeesListFragment;

/**
 * This the abstract parent class for all the activities we need in our project that will initiate
 * all that we need so the navigation of our {@link android.app.Fragment} based app will be smooth
 */

public abstract class MVCActivity extends AppCompatActivity {

    /**
     * Get the name of the class for the purpose of logging
     *
     */
    private static final String LOG_TAG = MVCActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * This will inflate the container we want to add the fragments to
         *
         */
        setContentView(R.layout.activity_main);
        startFragment(Constants.FRAGMENT_TAG_EMPLOYEE_LIST, null);
    }

    /**
     * @param tag
     * @param object
     *
     * This method will centralize a way of creating fragments from scratch or fetching them
     * from memory once they are added to the stack and not GC'd
     *
     * As an alternative to this approach we can use Dagger Injection too
     *
     */
    public void startFragment(String tag, Object object) {

        /**
         * This is where we check whether we have teh fragment instance in the stack or the
         * fragment is null.
         *
         * If we have the instance in memory then fetch it from there
         *
         * If we don't have the instance then create it from scratch
         *
         */
        Fragment fragment = getFragmentManager().findFragmentByTag(tag);

        if (fragment == null) {

            /**
             * If the Fragment is null then go thru the switch in order to
             */
            switch (tag) {
                case Constants.FRAGMENT_TAG_EMPLOYEE_LIST:
                    fragment = new EmployeesListFragment();
                    break;
                case Constants.FRAGMENT_TAG_EMPLOYEE_DETAIL:
                    fragment = new EmployeeDetailFragment();
                    break;
            }
        }

        /**
         * Once we created the new instance of the fragment this is where we end to add it to the
         * stack and the container
         */
        if (fragment != null) {

            Log.d(LOG_TAG, "Starting" + tag);
            /**
             * @see FragmentManager#beginTransaction()
             * @see FragmentTransaction#replace(int, Fragment, String)
             */
            FragmentTransaction transaction = getFragmentManager().beginTransaction().replace(R.id.fragment_container_main, fragment, tag);
            /**
             * @see FragmentTransaction#addToBackStack(String)
             */
            transaction.addToBackStack(tag);
            /**
             * @see FragmentTransaction#commit()
             */
            transaction.commit();
        }

    }
}
