package com.ad60.employeedir.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ad60.employeedir.controller.BaseModel;

/**
 * This is the parent fragment to all the fragments
 *
 * Created by ktadesse on 7/30/16.
 */
public abstract class MVCFragment <MODEL extends BaseModel>  extends Fragment{

    protected   MODEL mMVCFragmentModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         * Inflate {@link Fragment} specific view
         */
        View view = inflateChildView(inflater, container, savedInstanceState);
        /**
         * Inject all the NON-UI dependency injection we need for the fragments here.
         *
         * These no MAGIC place that we need to inject but since this is a parent to all we will
         * need our injections here and this method {@link Fragment#onCreateView(LayoutInflater, ViewGroup, Bundle)}
         * will always be called independent on the fragment is fetched from the memory or created new
         * from scratch
         *
         */
        injectNonUiDependencies();
        /**
         * <p>
         *      Here we can inject all the Ui dependecies specially of the {@link butterknife.ButterKnife}
         *      ones. The view holders can be injected once so we can hold on to all the view holders
         *      as long as our application dies. But for now we will inject the views for only the
         *      life cycles of the fragment.
         * </p>
         */
        injectUiDependencies(view);
        /**
         * This will return the Fragment's child view when ever a specific sub-class is needed to be
         * constructed out of the memory or a brand new from scratch
         */
        return view;
    }

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     *
     * These are for inflating the individual layouts of Fragments in the app
     */
    protected abstract View inflateChildView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    /**
     * @param view this will be used individual fragment View dependency injection
     */
    protected abstract void injectUiDependencies(View view);

    /***
     * All the NON-UI dependency injections that are shared globally will be injected here
     *
     */
    private void injectNonUiDependencies() {
        /**
         * When we first come here this is NULL after that it will depend on the lifecycle of the
         * fragments and the parent activity
         *
         * To be safe always we ask if this model is null or not
         *
         */
        if(mMVCFragmentModel == null) mMVCFragmentModel = (MODEL) new BaseModel();

        /**
         * Using the {@link BaseModel} check to see if we have dependency is injected or not
         * Because the {@link MVCFragment#onCreateView(LayoutInflater, ViewGroup, Bundle)} is going
         * to be called always we need to make sure that we don't re-inject once we inject it for all
         * the sub-Fragments of this class
         */
        if(!mMVCFragmentModel.isDependecyInjected()) {

            //TODO : inject all the globally shared injections here
        }
    }
}
