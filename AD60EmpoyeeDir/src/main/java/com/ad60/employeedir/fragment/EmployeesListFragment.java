package com.ad60.employeedir.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ad60.employeedir.R;
import com.ad60.employeedir.adapter.EmployeeListAdapter;
import com.ad60.employeedir.controller.BaseModel;
import com.ad60.employeedir.employee.EmployeeController;
import com.ad60.employeedir.module.Injector;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * This class will provide the UI for the list of employees that we want to see
 *
 * Created by ktadesse on 7/30/16.
 */
public class EmployeesListFragment extends MVCFragment<BaseModel> {

    private static final String LOG_TAG = EmployeesListFragment.class.getSimpleName();

    @Inject EmployeeController employeeController;

    @Override
    protected View inflateChildView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         * Inflate the {@link EmployeesListFragment} view here.
         *
         */
        return inflater.inflate(R.layout.employee_list_fragment, container, false);
    }

    @Override
    protected void injectUiDependencies(View view) {
        /**
         * This injection will always provide a new {@link com.ad60.employeedir.viewholder.EmployeesListViewHolder}
         *
         * We can make the injection a static one for this but really we have an Android concept of not
         * to hold static references to views or {@link android.content.Context}
         *
         */

        /**
         * This will set up the view we need for the individual fragments
         *
         */
        new Injector().getmViewHolderComponet().inject(this);
        new EmployeesListViewHolder(view);
    }

    /**
     * This is a view holder class that will let us handle UI logic for {@link com.ad60.employeedir.fragment.EmployeesListFragment}
     *
     *
     */
    public class EmployeesListViewHolder {

        @Bind(R.id.employee_list) ListView listView;

        public EmployeesListViewHolder(View view) {
            /**
             * @param view is the one that tells {@link ButterKnife} what view to inflate, in this case
             *             we will inflate {@link com.ad60.employeedir.R.layout.employee_list_fragment}
             */
            ButterKnife.bind(this, view);

            listView.setAdapter(new EmployeeListAdapter(employeeController.getEmployeesList()));

        }
    }

}
