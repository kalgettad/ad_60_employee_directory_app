package com.ad60.employeedir.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ad60.employeedir.Util.ClassTypes;

/**
 * TODO: this is a work in progress
 *
 * This Fragment will present the detail of each employee when we select from the list
 *
 * Created by ktadesse on 7/30/16.
 */
public class EmployeeDetailFragment extends MVCFragment {


    @Override
    protected View inflateChildView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null;
    }

    @Override
    protected void injectUiDependencies(View view) {}
}
