package com.ad60.employeedir.controller;

import com.ad60.employeedir.employee.EmployeeController;
import com.ad60.employeedir.employee.EmployeeControllerImpl;
import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;


/**
 * Created by ktadesse on 7/31/16.
 */
public class EmployeeConrollerTest {

    private EmployeeController employeeController;

    @Before
    public void setup(){
       employeeController = new EmployeeControllerImpl();
    }

    @Test
    public void size_isCorrect(){
        assertEquals(employeeController.getEmployeesList().size(), 2);
    }
}
